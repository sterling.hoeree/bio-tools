#!/usr/bin/env python

import os
import sys
import argparse

import logging

from Bio.Nexus import Nexus
from Bio import SeqIO

logging.basicConfig(level=logging.INFO)

data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.data')


def parse_args(parser=argparse.ArgumentParser(description=__doc__)):
    parser.add_argument('-t', '--input-type', type=str, default=None, choices=['fasta', 'nexus'],
                        help="The input file(s) type (by default, guess using the extensions fas or nex)")
    parser.add_argument('-o', '--output-file', type=str, default=None,
                        help="The output nexus file name. If not given, use stdout")
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help="Make the output of the program more verbose")
    parser.add_argument('input_file', nargs='+', help="The input files to concatenate, by species")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    nexi = []
    for input_file in args.input_file:
        nex = Nexus.Nexus()
        root, ext = os.path.splitext(input_file)
        logging.debug("Read file, ext: {}, {}".format(root, ext))
        file_type = 'fasta' if ext.lower().startswith('.fas') else 'nexus' if ext.lower().startswith('.nex') else None

        assert file_type, "Unknown extension for: " + input_file
        logging.debug("File type is {}".format(file_type))

        with open(input_file) as f:
            logging.debug("Opening {}".format(input_file))
            for seq in SeqIO.parse(handle=f, format=file_type):
                nex.add_sequence(name=seq.id, sequence=str(seq.seq))
                logging.debug("\tAdding sequence: {}".format(seq.id))

        nex_name = os.path.basename(root).split('.')[0]
        nexi.append((nex_name, nex,))
        logging.debug("Added nexus matrix: {}".format(nex_name))

    logging.debug("Writing {} nexus matrices".format(len(nexi)))
    output = open(args.output_file, 'w') if args.output_file else sys.stdout
    nex_combined = Nexus.combine(matrices=nexi)

    nex_combined.write_nexus_data(output, mrbayes=True)
    output.close()
