#!/usr/bin/env python

import os
import sys
import re
import csv
import argparse
import logging

from Bio import Phylo

logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser(description="Utility for selecting sections of a fasta file")
    parser.add_argument('-f', '--tree-format', type=str, default='newick',
                        help="Format of the tree file (default: newick)")
    parser.add_argument('-o', '--output-file', type=str, default=None,
                        help="The output file name. If not given, use stdout")
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help="Make output verbose")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    tree = Phylo.read(sys.stdin, args.tree_format.lower())
    for t in tree.get_terminals():
        t.name = '_'.join(t.name.split()[:2])

    output = open(args.output_file, 'w') if args.output_file else sys.stdout
    Phylo.write([tree], output, args.tree_format)
    output.close()
