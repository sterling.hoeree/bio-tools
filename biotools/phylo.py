#!/usr/bin/env python

import sys
import os
import shlex
import logging
import argparse
import shutil

from Bio import AlignIO
from Bio import Phylo
from Bio.Align.Applications import MuscleCommandline
from Bio.Phylo.Applications import PhymlCommandline

import process


logging.basicConfig(level=logging.DEBUG)
scratch_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.data')


def muscle_align(input_file_name):
    output_file_name = os.path.join(scratch_dir, '{}.aln'.format(os.path.basename(input_file_name)))

    cline = MuscleCommandline(input=input_file_name, out=output_file_name, clw=True)
    logging.debug("Executing: {}".format(str(cline)))

    process.open_subprocess(args=shlex.split(str(cline)))
    return output_file_name


def paml_analyse(input_file_name, in_file_format=None):
    in_file_format = in_file_format or \
                     'fasta' if input_file_name.endswith('.fas') or input_file_name.endswith('.fasta') \
        else 'clustal'

    output_phy_file_name = os.path.join(scratch_dir,
        os.path.basename(input_file_name.replace('.aln', '.phy') \
            if input_file_name.endswith('.aln') else '{}.phy'.format(input_file_name)))

    logging.debug("Doing align conversion {} -> {}".format(input_file_name, output_phy_file_name))
    AlignIO.convert(in_file=input_file_name, in_format=in_file_format,
                    out_file=output_phy_file_name, out_format='phylip-relaxed')

    cline = PhymlCommandline(input=output_phy_file_name, datatype='nt', model='GTR', alpha='e', bootstrap=100)
    logging.debug("Executing: {}".format(str(cline)))

    process.open_subprocess(args=shlex.split(str(cline)))

    return os.path.join(scratch_dir, '{}_phyml_tree.txt'.format(input_file_name))


def parse_args():
    parser = argparse.ArgumentParser(description="Utility for creating phylogenetic trees from fasta files")
    parser.add_argument('-i', '--input-fasta-file', type=str, default=None,
                        help="The input fasta file to use. If --aligned is not set, then it is assumed to be aligned. "
                             "If this isn't given, then the fasta file is read from stdin")
    parser.add_argument('-a', '--align', action='store_true', default=False,
                        help="Align the --input-fasta-file with MUSCLE before attempting to create the tree")
    parser.add_argument('-o', '--output-file-name', type=str,
                        default=os.path.join(os.path.abspath(os.getcwd()), 'phyml_tree.txt'),
                        help="The name of the output tree file to use. If not given, will be 'phyml_tree.txt'")
    return parser.parse_args()


if __name__ == '__main__':
    if not os.path.exists(scratch_dir):
        os.mkdir(scratch_dir)

    args = parse_args()
    input_data_h = sys.stdin if not args.input_fasta_file else open(args.input_fasta_file)
    fasta = input_data_h.read()
    input_data_h.close()

    fasta_file_name = os.path.join(scratch_dir, 'data.fas')
    with open(fasta_file_name, 'w') as f:
        f.write(fasta)
    logging.debug("Wrote {}".format(fasta_file_name))

    if args.align:
        logging.debug("Doing alignment")
        fasta_file_name = muscle_align(fasta_file_name)
        logging.debug("Wrote alignment to {}".format(fasta_file_name))

    logging.debug("Doing PAML analysis")
    tree_file_name = paml_analyse(fasta_file_name)
    shutil.copyfile(tree_file_name, args.output_file_name)
    logging.debug("Copied tree file {} -> {}".format(tree_file_name, args.output_file_name))

    logging.debug("Reading tree file {}".format(args.output_file_name))
    tree = Phylo.read(args.output_file_name, "newick")
    Phylo.draw_ascii(tree)

    # shutil.rmtree(scratch_dir)
