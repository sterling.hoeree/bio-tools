#!/usr/bin/env python

import os
import sys
import re
import csv
import argparse
import logging

from Bio import SeqIO

logging.basicConfig(level=logging.INFO)

data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.data')


def read_plant_list(col_index, equals):
    results = []
    with open(args.input_species_list) as f:
        reader = csv.reader(f)
        headers = reader.next()
        for row in reader:
            logging.debug("Read raw row: {}".format(row))
            species_name = row[0].strip().lower().capitalize().replace('.', '_').replace(' ', '_')
            if not species_name:
                logging.debug("No species name")
                continue
            filter_col_contents = row[col_index].split('.')[0].replace('>', '').lower()
            logging.debug("Checking {} against {}".format(filter_col_contents, equals))
            if filter_col_contents == 'na':
                logging.debug('{}:{} is NA'.format(species_name, headers[col_index]))
                continue
            if filter_col_contents == equals.lower():
                logging.debug("  It's a match!!")
                results.append(species_name)

    return results


def parse_args():
    parser = argparse.ArgumentParser(description="Utility for selecting sections of a fasta file")
    parser.add_argument('-i', '--input-species-list', type=str,
                        help="Use this file as the full list of plant species")
    parser.add_argument('-c', '--lookup-column', type=int, default=1,
                        help="Use this column index (starting from 1) to filter the species from the fasta file. "
                             "Note that the species names are assumed to be in the 0th column")
    parser.add_argument('-e', '--equals', type=str, required=True,
                        help="What the --lookup-column value should equal for each row for the filter to succeed")
    parser.add_argument('-o', '--output-file', type=str, default=None,
                        help="The output fasta file name. If not given, use stdout")
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help="Make output verbose")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    filtered = read_plant_list(args.lookup_column, args.equals.lower())
    logging.debug("Filtered: {}".format(filtered))
    ids_seen = set()
    records = []
    for r in SeqIO.parse(sys.stdin, 'fasta'):
        logging.debug("Reading {}".format(r.id))
        # desc = extract_description(r)
        # logging.debug("  Description: {}".format(desc))
        # desc_dict = {'sp': desc[0]}
        # desc_dict.update({v.split(':')[0]: v.split(':')[1] for v in desc[1:]})

        if r.id in filtered:
            logging.debug("  Match")
            records.append(r)

    def compare_records(r0, r1):
        return 0 if r0.id == r1.id else -1 if r0.id < r1.id else 1
    records.sort(cmp=compare_records)

    output = open(args.output_file, 'w') if args.output_file else sys.stdout
    SeqIO.write(sequences=records, handle=output, format='fasta')
    output.close()
