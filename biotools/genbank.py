#!/usr/bin/env python
"""
Query and fetch genbank files from the 'nucleotide' database by species and gene.

:authors: Sterling Hoeree
"""

import sys
import os
import logging
import json
import argparse
from StringIO import StringIO

from Bio import SeqIO, Entrez

logging.basicConfig(level=logging.DEBUG)

data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.data')


class NoResults(Exception):
    pass


def mkdirp(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if not e.errno == 17:
            raise


def fetch(input_species, input_genes, data_path, query_str=None, state=None):
    state = state or {}
    failures = []

    query_str_id = None
    if query_str:
        query_str_id = query_str.split(':')[1]
        logging.info("Mapping query string {} to id: {}".format(query_str, query_str_id))

    for sp in input_species:
        saving_accessions = []

        try:
            identifiers = input_genes + [query_str_id]
            already_searched = state.get(sp, None)
            if already_searched:
                already_searched = [str(x) for x in already_searched.keys()]
                logging.debug("Already searched for {} for species {}".format(already_searched, sp))
                identifiers = filter(lambda x: x.lower() not in already_searched, identifiers)
                logging.info("{} left to find for {}".format(identifiers, sp))

            if not identifiers:
                logging.info("{}: all identifiers/genes already found".format(sp))
                continue

            search_term = '{} [organism] '.format(sp)

            genes_to_search = filter(lambda x: x in identifiers, input_genes)
            if genes_to_search:
                search_term += 'AND (' + ' OR '.join('{} [gene]'.format(x) for x in genes_to_search)
            if query_str and query_str_id in identifiers:
                search_term += (' OR ' if genes_to_search else '') + '"' + query_str.split(':')[0] + '"[all]'

            search_term += ')'

            logging.info("Searching for '{}'".format(search_term))

            try:
                handle = Entrez.esearch(db='nucleotide', term=search_term, retmax=10000)
                record = Entrez.read(handle)
                logging.debug("{}: Found {} records".format(search_term, record[u'Count']))
                handle.close()

                saving_accessions = []
                if int(record[u'Count']) > 0:
                    for id in record[u'IdList']:
                        # TODO: don't fetch if id already retrieved before?
                        handle = Entrez.efetch(db='nucleotide', id=id, rettype='gb', retmode='text')
                        gb = handle.read()
                        handle.close()

                        seq = SeqIO.read(StringIO(gb), 'genbank')

                        try:
                            gene = filter(lambda x: x.type == 'gene', seq.features)[0].qualifiers['gene'][0].lower()
                        except Exception as e:
                            logging.info("Could not retrieve 'gene' feature for {}: {}".format(sp, id))
                            if not query_str_id:
                                gene = 'unk'
                            else:
                                logging.info("Using query string id {} in place of a gene".format(query_str_id))
                                gene = query_str_id

                        if 'like' in seq.description:
                            logging.warn("Skipping possibly bad '{}-like' sequence: {}".format(gene, id))
                            continue

                        gene_dir = os.path.join(data_path, gene)
                        mkdirp(gene_dir)
                        file_name = os.path.join(gene_dir, '{}.{}.gb'.format(sp, seq.id))
                        with open(file_name, 'w') as f:
                            logging.info("Saved data in {}".format(file_name))
                            f.write(gb)

                        saving_accessions.append((gene, seq.id))

                    if sp not in state:
                        state[sp] = {}
                    for gene, seq_id in saving_accessions:
                        if gene not in state[sp]:
                            state[sp][gene] = []
                        state[sp][gene].append(seq_id)

                else:
                    logging.info("No results for {}!".format(sp))

                    msg = ''
                    if u'ErrorList' in record:
                        errorlist = record[u'ErrorList']
                        msg = ';'.join(['{}: {}'.format(err, ';'.join(errorlist[err]))
                                        for err in errorlist if errorlist[err]])
                    raise NoResults(msg)

            except Exception as e:
                logging.exception(e.message)
                failures.append((sp, search_term, e.__class__.__name__, e.message))

        except KeyboardInterrupt:
            logging.info("User interrupted process, saving results and exiting")

            if saving_accessions:
                if sp not in state:
                    state[sp] = {}
                for gene, seq_id in saving_accessions:
                    if gene not in state[sp]:
                        state[sp][gene] = []
                    if seq_id not in state[sp][gene]:
                        state[sp][gene].append(seq_id)

            logging.info("Current state was persisted")
            break

    return state, failures


def query(species, genes, query_str=None):
    global data_path
    mkdirp(data_path)
    logging.info("Saving files to {}".format(data_path))

    state = {}
    try:
        with open(os.path.join(data_path, 'state.json')) as f:
            state = json.load(f)
    except (OSError, IOError):
        logging.info("No previous state.json file")
        pass

    state, failures = fetch(input_species=species, input_genes=genes, query_str=query_str,
                            data_path=data_path, state=state)
    with open(os.path.join(data_path, 'results.csv'), 'w') as results_f, \
            open(os.path.join(data_path, 'state.json'), 'w') as state_f:
        for sp in state:
            for gene in state[sp]:
                for seq_id in state[sp][gene]:
                    results_f.write('{},{},{}\n'.format(sp, gene, seq_id))
        json.dump(state, fp=state_f, indent=2, sort_keys=True)

    with open(os.path.join(data_path, 'FAILED.csv'), 'w') as f:
        for sp, search, e, msg in failures:
            f.write('{},{},{},{}\n'.format(sp, search, e, msg))


def parse_from(directory):
    parsed = {}

    gene_dirs = filter(os.path.isdir, [os.path.join(directory, x) for x in os.listdir(directory)])
    logging.debug("Parsing dir: {}".format(gene_dirs))

    gene_d_normalised = {}
    for gene_d in gene_dirs:
        gene = gene_d.lower()
        if gene not in gene_d_normalised:
            gene_d_normalised[gene] = []

        gb_files = [os.path.join(gene_d, x) for x in
                    filter(lambda f: os.path.isfile(os.path.join(gene_d, f)) and f.endswith('.gb'),
                           os.listdir(gene_d))]
        gene_d_normalised[gene] = gene_d_normalised[gene] + gb_files

    for gene_level in gene_d_normalised:
        gene = os.path.basename(gene_level)

        record_for_species = {}
        for gb in gene_d_normalised[gene_level]:
            logging.debug("Parsing {}".format(gb))
            sp = os.path.basename(gb).split('.')[0]
            with open(gb, 'rU') as gb_f:
                for record in SeqIO.parse(gb_f, 'genbank'):
                    if 'like' in record.description:
                        logging.warn("Skipping {}-like gene: {}".format())
                    l = len(record)
                    if sp not in record_for_species:
                        record_for_species[sp] = record
                    elif l > len(record_for_species[sp]) and \
                                    'unverified' not in record.description.lower() and \
                                    'partial' not in record.description.lower():
                        record_for_species[sp] = record

        to_write = []
        for sp in record_for_species:
            r = record_for_species[sp]
            logging.info("Choosing this record to write: {}".format(r))
            to_write.append(r)

        fasta = StringIO()
        SeqIO.write(sequences=to_write, handle=fasta, format='fasta')
        parsed[gene.lower()] = {'count': len(to_write), 'fasta': fasta.getvalue(),
                                'species': dict(set((x, record_for_species[x].id,) for x in record_for_species.keys()))}
        logging.info("Wrote {} records for gene {}".format(len(to_write), gene.lower()))

    with open(os.path.join(directory, 'parsed.json'), 'w') as f:
        json.dump(obj=parsed, fp=f, indent=2, sort_keys=True)
        logging.info("Saved {}".format(f.name))

    for gene in parsed:
        with open(os.path.join(directory, '{}.fas'.format(gene)), 'w') as f:
            f.write(parsed[gene]['fasta'])
            logging.info("Saved {}".format(f.name))

    return parsed


def parse_args():
    parser = argparse.ArgumentParser(description="Utility for fetching and parsing GenBank data")
    parser.add_argument('-q', '--query', action='store_true', default=False,
                        help="Query for species, using the --query-genes list of genes and reading from "
                             "stdin or --input-species-list")
    parser.add_argument('-s', '--query-str', type=str, default=None, metavar='string:identifier',
                        help="Query for species but use this Genbank query string OR'd with the input set of genes to "
                             "find sequences")
    parser.add_argument('-g', '--genes', type=str, default=None, metavar='gene0,gene1,...,geneN',
                        help="Use these genes for querying or producing the output CSV")
    parser.add_argument('-p', '--parse-from-dir', type=str, dest='parse_from_dir',
                        help="Parse a directory of GenBank (.gb) files, producing fasta files")
    parser.add_argument('-i', '--input-species-list', type=str, default=None,
                        help="Use this file as the full list of plant species; if not given for --query, use stdin")
    parser.add_argument('-c', '--produce-full-csv', type=str, default=None,
                        help="Produce a full CSV with this name joining the missing and found accession numbers "
                             "for species")
    parser.add_argument('-e', '--entrez-email', type=str, default=os.environ.get('ENTREZ_EMAIL', None),
                        help="Defaults to the env variable ENTREZ_EMAIL")

    return parser.parse_args()


def get_species_from_stream(stream):
    species_list = []
    for line in stream.readlines():
        vals = line.strip().split(',')
        sp = vals[0].replace('.', ' ').strip()
        if sp:
            species_list.append(sp)
        else:
            logging.debug("Ignoring input line: {}".format(line.strip()))

    return species_list


if __name__ == '__main__':
    args = parse_args()
    if not args.entrez_email:
        raise ValueError('--entrez-email or env variable ENTREZ_EMAIL is required')

    Entrez.email = args.entrez_email
    species_list = None

    if args.query:
        if not args.genes:
            raise ValueError("--genes must be given for --query to work")

        if args.input_species_list:
            stream = open(args.input_species_list)
        else:
            stream = sys.stdin

        species_list = get_species_from_stream(stream)
        stream.close()

        genes = args.genes.strip().split(',')
        logging.info("Searching for genes: {}".format(genes))

        query(species=species_list, genes=genes, query_str=args.query_str)


    parsed_state = {}
    if args.parse_from_dir:
        logging.debug("Parsing from dir: {}".format(args.parse_from_dir))
        parsed_state = parse_from(directory=args.parse_from_dir)

    if args.produce_full_csv:
        if not args.genes:
            raise ValueError("--query-genes must be given for --produce-full-csv to work")

        if not parsed_state:
            try:
                with open(os.path.join(data_path, 'parsed.json')) as f:
                    parsed_state = json.load(fp=f)
            except:
                raise ValueError("--parse-from-dir must be run at least once for --produce-full-csv to work")

        if not species_list:
            if args.input_species_list:
                stream = open(args.input_species_list)
            else:
                logging.info("Please enter the master list of species to use, separated by newlines (CTRL+D to stop):")
                stream = sys.stdin

            species_list = get_species_from_stream(stream)
            stream.close()

        with open(args.produce_full_csv, 'w') as csv_f:
            logging.info("Generating a CSV from parsed state: {}".format(csv_f.name))
            genes = args.genes.strip().split(',')
            csv_f.write(','.join(['Species'] + genes) + '\n')
            for sp in species_list:
                logging.info("Looking up {}".format(sp))
                csv_line = [sp]
                for gene in genes:
                    gene = gene.lower()
                    if gene not in parsed_state:
                        logging.error("No species for gene {} were parsed at all".format(gene))
                        csv_line.append('NA')
                        continue

                    if sp not in parsed_state[gene]['species']:
                        logging.debug("{}: {} was not parsed".format(sp, gene))
                        csv_line.append('NA')
                    else:
                        logging.debug("{}: adding {}".format(sp, gene))
                        csv_line.append(parsed_state[gene]['species'][sp])

                csv_f.write(','.join(csv_line) + '\n')
