#!/usr/bin/env python
"""
In a FASTA file, find the best reading frame for each sequence and write it out.

:authors: Bruno Perriot, Sterling Hoeree

"""

import sys
import argparse
import logging

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna


logging.basicConfig(level=logging.DEBUG)


start_list = [
    'ATG',
]

stop_list = [
    'TAA',
    'TAG',
    'TGA',
]

dict_ = {
    'AGG': 'R',
    'ACT': 'T',
    '': 'P',

}


def reverse_sequence(seq):
    s = seq[::-1]
    s = s.replace('A', '_')
    s = s.replace('T', 'A')
    s = s.replace('_', 'T')
    s = s.replace('C', '_')
    s = s.replace('G', 'C')
    s = s.replace('_', 'G')
    return s


def out_possible_sequences(seq):
    yield seq[:]
    yield seq[1:]
    yield seq[2:]
    s = reverse_sequence(seq)
    yield s[:]
    yield s[1:]
    yield s[2:]


def score_sequence(seq):
    seq_lenght = [0]
    while len(seq) >= max(seq_lenght) * 3 and len(seq) > 3:
        len_ = 0
        code = ''
        while code != 'ATG' and len(seq) > 3:
            code, seq = seq[:3], seq[3:]
        while code not in stop_list and len(seq) > 3:
            code, seq = seq[:3], seq[3:]
            len_ += 1
        seq_lenght.append(len_)
    seq_lenght.append(len_)
    return max(seq_lenght)


def parse_args(parser=argparse.ArgumentParser(description=__doc__)):
    parser.add_argument('-o', '--output-file', type=str, default=None,
                        help="The output fasta file name. If not given, use stdout")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    metad = open('{}.csv'.format(args.output_file or 'counts'), 'w')

    records = []
    for r in SeqIO.parse(sys.stdin, 'fasta'):
        logging.debug("Processing {}...".format(r.id))
        comment = r.description
        sequence = str(r.seq.upper())
        sequence = sequence.replace('-', '')
        ps = list(out_possible_sequences(sequence))
        scores = [score_sequence(s) for s in ps]
        scores.index(max(scores))
        best = ps[scores.index(max(scores))]

        logging.info("Best score was {}".format(max(scores)))
        logging.debug("Best sequence: {}...".format(best[:20]))
        r.seq = Seq(best, generic_dna)
        metad.write(comment.rstrip() + ',')
        metad.write(','.join([str(j) for j in scores]) + '\n')

        records.append(r)

    f = open(args.output_file, 'w') if args.output_file else sys.stdout
    SeqIO.write(records, f, 'fasta')
    f.close()
