#!/usr/bin/env python

import os
import sys
import re
import csv
import argparse
import sqlite3

import logging

from Bio import SeqIO

logging.basicConfig(level=logging.INFO)

data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.data')


NULL = 'NULL'
re_accession = re.compile(r"ACCESSION:([^ _]*)")
re_genus = re.compile(r"GENUS:([^ _]*)")
re_epithet = re.compile(r"EPITHET:([^ _A-Z]*)")
re_hybrid = re.compile(r"HYBRID:([^ _]*)")
re_variety = re.compile(r"VARIETY:([^ _]*)")
re_subspecies = re.compile(r"SUBSPECIES:([^ _]*)")
re_region = re.compile(r"REGION:([^ _]*)")
re_synonym = re.compile(r"SYNONYM:([^ ]*)")
re_flags = re.compile(r"FLAGS:([^ ]*)")
re_source = re.compile(r"SOURCE:\"(.*)\"")


def extract_properties(record):
    desc = record.description
    props = {}

    props['name'] = r.id

    p = re_accession.findall(desc)
    assert p, "ACCESSION not found in {}".format(record.id)
    props['accession'] = p[0]

    p = re_genus.findall(desc)
    assert p, "GENUS not found in {}".format(record.id)
    props['genus'] = p[0]

    p = re_epithet.findall(desc)
    props['epithet'] = p[0] if p else NULL

    p = re_hybrid.findall(desc)
    props['hybrid'] = p[0] if p else NULL

    p = re_variety.findall(desc)
    props['variety'] = p[0] if p else NULL

    p = re_subspecies.findall(desc)
    props['subspecies'] = p[0] if p else NULL

    p = re_region.findall(desc)
    props['region'] = p[0] if p else NULL

    p = re_synonym.findall(desc)
    props['synonym'] = p[0] if p else NULL

    p = re_flags.findall(desc)
    flags = p[0].split(",") if p else []
    props['short_genus'] = int('short_genus' in flags)
    props['unverified'] = int('unverified' in flags)

    p = re_source.findall(desc)
    assert p, "SOURCE not found in {}".format(record.id)
    props['source'] = p[0]

    props['dna'] = str(r.seq).upper()

    return props


def get_db(db_name):
    logging.debug("Connect to db: {}".format(db_name))
    db = sqlite3.connect(db_name)

    sql = """
    CREATE TABLE IF NOT EXISTS data (
      accession TEXT PRIMARY KEY,
      name TEXT NOT NULL,
      genus TEXT NOT NULL,
      epithet TEXT NULL DEFAULT NULL,
      hybrid TEXT NULL DEFAULT NULL,
      variety TEXT NULL DEFAULT NULL,
      subspecies TEXT NULL DEFAULT NULL,
      region TEXT NULL DEFAULT NULL,
      synonym TEXT NULL DEFAULT NULL,
      short_genus INTEGER DEFAULT 0,
      unverified INTEGER DEFAULT 0,
      `order` TEXT NULL DEFAULT NULL,
      family TEXT NULL DEFAULT NULL,
      source TEXT NOT NULL,
      gene TEXT NOT NULL,
      dna TEXT NOT NULL
    )"""

    logging.debug("Executing sql: {}".format(sql))
    db.execute(sql)
    db.commit()

    return db


def get_insert_statement(values):
    values_order = ["accession", "name", "genus", "epithet", "hybrid", "variety","subspecies", "region", "synonym",
                    "short_genus", "unverified", "source", "gene", "dna"]
    values_tuple = []
    for o in values_order:
        if o in values:
            values_tuple.append(values[o])
        else:
            values_tuple.append(None)
    values_tuple = tuple(values_tuple)

    return (
      "INSERT OR IGNORE INTO data " +
      "(accession, name, genus, epithet, hybrid, variety, "
      "subspecies, region, synonym, short_genus, unverified, source, gene, dna) " +
      "VALUES (" + ','.join(["?"] * len(values_order)) + ")",
      values_tuple)


def parse_args():
    parser = argparse.ArgumentParser(description="Utility for working with fasta data/metadata and databases")
    parser.add_argument('-i', '--input-species-list', type=str,
                        help="The master csv of species")
    parser.add_argument('-d', '--database', type=str, required=True,
                        help="The path to the (sqlite3) database to use")
    parser.add_argument('-g', '--gene', type=str, required=True,
                        help="The gene that this data is for (e.g. matK, rbcL)")
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help="Make output verbose")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    logging.debug("Connecting to sqlite3 database {}".format(args.database))
    db = get_db(args.database)

    for r in SeqIO.parse(sys.stdin, 'fasta'):
        logging.debug("Reading {}".format(r.description))

        properties = extract_properties(r)
        properties.update({'gene': args.gene.lower()})
        sql = get_insert_statement(properties)

        logging.debug("Executing on db: {}".format(sql))
        db.execute(*sql)

    db.commit()
    db.close()
