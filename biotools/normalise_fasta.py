#!/usr/bin/env python

import os
import sys
import re
import csv
import argparse
import logging

from Bio import SeqIO

logging.basicConfig(level=logging.INFO)

data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.data')


# Flags
class Flags(object):
    short_genus = 'short_genus'
    unverified = 'unverified'


# Only extract SOURCE from a MEGA-wrecked description
tags = {'REGION', 'FLAGS', 'ACCESSION', 'SYNONYM', 'EPITHET', 'GENUS', 'VARIETY', 'SOURCE', 'HYBRID', 'SUBSPECIES'}
def make_mega_re(search_tag='SOURCE'):
    search_until = tags - {search_tag}
    return re.compile('{}:(.+(?=_({}):))'.format(search_tag, '|'.join(search_until)))


def make_species_name(genus, epithet='NA', hybrid=None, subsp=None, variety=None):
    return genus + (('_' + epithet) if epithet and epithet != 'NA' else '') + \
              (('_x_' + hybrid) if hybrid else '') + \
              (('_subsp._' + subsp) if subsp else '') + \
              (('_var._' + variety) if variety else '')


def extract_description(record, override_accession_number=None):
    if '_GENUS' in record.description or '_ACCESSION' in record.description or '_SOURCE' in record.description:
        # MEGA wrecked everything by adding underscores everywhere and maybe cutting off text
        logging.warn("Possibly wrecked record metadata for: {}".format(record.id))

        source = make_mega_re('SOURCE').findall(record.description)
        if not source:
            logging.warn("  Description is wrecked by mega?")
            record.description = record.description[(record.description.find("SOURCE:") + len("SOURCE:")):]
        else:
            record.description = source[0][0]

        accession = make_mega_re('ACCESSION').findall(record.description)
        if accession:
            if not record.description.startswith(accession):
                logging.warn("  Source does not include accession number! Adding it in: {}".format(accession))
                record.description = "{} {}".format(accession, record.description)
        else:
            logging.warn("  There isn't an accession number for {}!".format(record.id))
            if record.description.startswith(record.id.split('_')[0]):
                logging.warn("  It's possible the accession number was lost, and we don't want {} to be the accession. "
                             "Will replace with UNKNOWN.".format(record.description.split()[0]))
                record.description = "UNKNOWN {}".format(record.description)

        logging.info("  Replaced {} description with: {}".format(record.id, record.description))

    splits = re.split(r' |_', record.description)

    logging.debug("Split record description: {}".format(splits))

    if 'GENUS:' in record.description:
        # This was probably already modified by this function
        logging.debug("Probably already modified by this script")
        splits = ' '.join(splits).replace(': ', ':').split(' ')  # Change e.g. "GENUS: Buglium" to "GENUS:Buglium"

        sp = '_'.join(splits[:2])
        rest = splits[2:]
        if 'GENUS:' in sp or 'FLAGS:' in sp:
            # Probably just a genus without an epithet
            sp = splits[0]
            rest = splits[1:]

        return [sp] + rest

    accession = splits[0]
    flags = []
    subsp = []
    variety = []
    hybrid = []
    region = []

    # Handle weird accession number that starts with 'NC_'
    if splits[0].lower() == 'nc':
        accession = '_'.join(splits[0:2])
        splits = [accession] + splits[2:]

    assert accession
    logging.debug("Extracted accession number: {}".format(accession))

    if '.' not in accession and override_accession_number:
        accession = override_accession_number
        splits = [override_accession_number] + splits
        logging.debug("Overriding accession with {} instead".format(accession))

    # Handle the case when the accession number shows a range of nucleotides, e.g. AM887492.1:730-2247
    if ':' in accession and '-' in accession:
        parts = accession.split(':')
        assert parts
        accession = parts[0]
        region = [parts[1]]
        logging.debug("Found a region in the accession number: {}".format(region))

    # Add a flag for the 'unverified' status
    if splits[1].lower().startswith('unverified'):
        flags.append(Flags.unverified)
        splits = [accession] + splits[2:]

    # Parse the rest of the line for genus, epithet, variety, subspecies, hyrbidization, etc.
    genus = splits[1]
    epithet = splits[2]

    # This could be a "simple" genus name, like "B." instead of "Buglium"
    # TODO: Need to expand this simplification later because the single letter is useless
    if '.' in splits[1]:
        spl = splits[1].split('.')
        genus = spl[0]
        epithet = spl[1]
        flags.append(Flags.short_genus)
        splits = [accession, genus, epithet] + splits[1:]

    # Parse the second part of the name, e.g. Buglium *x* buglibum or Buglium *sp.*
    if splits[2].lower() == 'x':
        epithet = 'NA'
        hybrid = [splits[3]]

    elif splits[2].lower() == 'sp.':
        # Only Genus is present, e.g. 'Buglium sp.'
        epithet = 'NA'

    # Parse the third part of the name, e.g. Buglium booglii *x* buglibum or Buglium booglii *var.* buglibum
    if len(splits) > 3 and splits[3].lower() == 'x':
        # Is a hybrid
        epithet = splits[2]
        hybrid = [splits[4]]

    elif len(splits) > 3 and splits[3].lower().startswith('sub'):
        # Is a subspecies
        epithet = splits[2]
        subsp = [splits[4]]

    elif len(splits) > 3 and splits[3].lower().startswith('var'):
        # Is a variety
        epithet = splits[2]
        variety = [splits[4]]

    # Put all of the pieces together
    descriptors = []
    descriptors.append('GENUS:' + genus)
    descriptors.append('EPITHET:' + epithet)
    descriptors.append('SOURCE:"{}"'.format(' '.join(record.description.split('_'))))

    if hybrid:
        descriptors.append('HYBRID:' + hybrid[0])
    if variety:
        descriptors.append('VARIETY:' + variety[0])
    if subsp:
        descriptors.append('SUBSPECIES:' + subsp[0])
    if region:
        descriptors.append('REGION:' + region[0])

    # Create the final name for the species, e.g. 'Buglium booglii var. buglibum'
    species = genus + (('_' + epithet) if epithet != 'NA' else '') + \
              (('_x_' + hybrid[0]) if hybrid else '') + \
              (('_subsp._' + subsp[0]) if subsp else '') + \
              (('_var._' + variety[0]) if variety else '')

    return [species.lower().capitalize()] + ['ACCESSION:' + accession] + \
           ((['FLAGS:' + ','.join(x.lower() for x in flags)]) if flags else []) + descriptors


def parse_args():
    parser = argparse.ArgumentParser(description="Utility for normalising the names of species for a fasta file")
    parser.add_argument('-i', '--input-species-list', type=str, default=None,
                        help="Use this file as the full list of plant species")
    parser.add_argument('-c', '--join-column', type=int, default=1,
                        help="Use this column index (starting from 1) to join the accession number against for "
                             "each species. Note that the species names are assumed to be in the 0th column")
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help="Make the output of the program more verbose")
    parser.add_argument('-u', '--guess-for-unknown-accessions', action='store_true', default=False,
                        help="Only allowed when using --input-species-list. If given, when an UNKNOWN accession "
                             "number is encountered, try to match the species name in the fasta file with the list "
                             "and substitute in the accession number for the --join-column")
    parser.add_argument('-o', '--output-file', type=str, default=None,
                        help="The output fasta file name. If not given, use stdout")
    parser.add_argument('-d', '--dont-autoadd', action='store_true', default=False,
                        help="Do NOT automatically try to add missing species (detected by checking the "
                             "--input-species-list accessions against what is in the source data)")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    ids_seen = set()
    records = []
    for r in SeqIO.parse(sys.stdin, 'fasta'):
        desc = extract_description(r)
        r.name = r.id = desc[0]
        r.description = ' '.join(desc)
        if r.id not in ids_seen:
            records.append(r)
            ids_seen.add(r.id)
        else:
            logging.warn("{} is a duplicate in the source data!".format(r.id))

    if args.input_species_list:
        join_col = args.join_column
        if join_col < 1:
            logging.error("Cannot have --join-column < 1; will be set to 1")
            join_col = 1

        with open(args.input_species_list) as f:
            # Create a dict of ACCESSION NUMBER -> SPECIES NAME
            reader = csv.reader(f)
            headers = reader.next()
            species_to_accession = {}
            accession_to_species = {}
            for row in reader:
                species_name = row[0].strip().lower().capitalize()\
                    .replace('.', '_').replace(' ', '_').replace('"','').replace("'",'')
                if not species_name:
                    continue
                join_col_contents = row[join_col].split('.')[0].replace('>', '')
                if join_col_contents.lower() == 'na':
                    logging.debug('{} is NA'.format(species_name))
                    continue
                accession_to_species[join_col_contents] = species_name
                species_to_accession[species_name] = join_col_contents

        record_manifest = {}
        prompt_for_guess = args.guess_for_unknown_accessions
        for r in records:
            accession_number = re.findall(r'ACCESSION:([^ ]*)', r.description)
            if len(accession_number) == 0:
                logging.warn("{} doesn't have an ACCESSION number!".format(r.id))
                continue

            if 'like' in r.description:
                logging.warn("{} is only a 'LIKE' gene!!".format(r.id))

            accession_number = accession_number[0].split('.')[0]

            if accession_number == 'UNKNOWN':
                logging.warn("{} has an UNKNOWN accession number.".format(r.id))
                if not prompt_for_guess:
                    logging.warn("I can try to guess which accession number should be there from the input species list, "
                                 "if you specify --guess-for-unknown-accessions")

                else:
                    for name in species_to_accession:
                        if r.id.lower().startswith(name.lower()) or name.lower().startswith(r.id.lower()):
                            logging.warn("Using accession number {} from species {} in plant list for {}"
                                         .format(species_to_accession[name], name, r.id))
                            accession_number = species_to_accession[name]
                            r.description = r.description.replace('UNKNOWN', species_to_accession[name])
                            logging.warn("New record description: {}".format(r.description))

            if accession_number in accession_to_species:
                name_to_use = accession_to_species[accession_number]
                if name_to_use != r.id:
                    synonym = r.id
                    r.id = r.name = name_to_use
                    r.description = ' '.join(r.description.split()[1:] + ['SYNONYM:' + synonym])

            record_manifest[accession_number] = r.id

        missing = {}
        for accession_number in species_to_accession.keys():
            if accession_number not in record_manifest:
                logging.warn("{} ({}) is missing from the fasta file!".format(accession_number, species_to_accession[accession_number]))
                missing[species_to_accession[accession_number]] = accession_number

        if args.dont_autoadd:
            logging.info("--dont-autoadd is set. Not adding missing species by searching data directory.")

        elif os.path.exists(data_path):
            data_files = {}
            for root, dirs, files in os.walk(data_path):
                for name in files:
                    acc = os.path.basename(name).split('.')[1]
                    data_files[acc] = os.path.join(root, name)
            for species_name in missing:
                accession_number = missing[species_name]
                logging.info("Checking if missing {} ({}) is in data path {}".format(accession_number,
                                                                                     species_name, data_path))
                if accession_number in data_files:
                    path = data_files[accession_number]
                    with open(path, 'rU') as f:
                        r = [r for r in SeqIO.parse(f, 'genbank')][0]
                        desc = extract_description(r, override_accession_number=accession_number)
                        r.name = r.id = desc[0]
                        r.description = ' '.join(desc)
                        records.append(r)

    def compare_records(r0, r1):
        return 0 if r0.id == r1.id else -1 if r0.id < r1.id else 1
    records.sort(cmp=compare_records)

    output = open(args.output_file, 'w') if args.output_file else sys.stdout
    SeqIO.write(sequences=records, handle=output, format='fasta')
    output.close()
