import sys
import os
import subprocess
import shlex
import select
import logging


logging.basicConfig(level=logging.DEBUG)


def stream_process_until_done(process):
    if os.name == 'posix' and (process.stdout or process.stderr):
        import fcntl
        if process.stdout:
            fcntl.fcntl(process.stdout.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)
        if process.stderr:
            fcntl.fcntl(process.stderr.fileno(), fcntl.F_SETFL, os.O_NONBLOCK)

    while True:
        ret = process.poll()
        if ret is not None:
            break

        fds = []
        if process.stdout:
            fds.append(process.stdout)
        if process.stderr:
            fds.append(process.stderr)

        stdout = stderr = ''
        if fds:
            ready = select.select(fds, [], [])

            for fd in ready[0]:
                if fd.fileno() == process.stdout.fileno():
                    stdout = process.stdout.read()
                elif fd.fileno() == process.stderr.fileno():
                    stderr = process.stderr.read()

        yield None, stdout, stderr

    stdout = stderr = ''
    if process.stdout:
        stdout = process.stdout.read()
    if process.stderr:
        stderr = process.stderr.read()

    yield ret, stdout, stderr


class ProcessException(Exception):
    def __init__(self, cmd, returncode, err, out):
        super(ProcessException, self).__init__('"{}": code={}\nstderr:\n{}\n----\nstdout:\n{}'.format(
            ' '.join(cmd) if type(cmd) == list else cmd, returncode, err, out))


def open_subprocess(args, stdout_h=sys.stdout, stderr_h=sys.stderr):
    command = args if isinstance(args, list) else shlex.split(args)

    shell_proc = None
    try:
        shell_proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                      close_fds=(os.name == 'posix'))

        min_bufsize = 180  # or '\n' seen
        all_out = all_err = []
        out_buf = err_buf = ''
        for ret, stdout, stderr in stream_process_until_done(shell_proc):
            if stdout:
                out_buf += stdout
                if len(out_buf) >= min_bufsize:
                    stdout_h.write(out_buf)
                    out_buf = ''
                elif '\n' in out_buf:
                    b = out_buf.split('\n')
                    if len(b) > 1:
                        for line in b[:-1]:
                            stdout_h.write(line)
                        out_buf = b[-1]
                    else:
                        stdout_h.write(b[0])
                        out_buf = ''

                all_out.append(stdout)

            if stderr:
                err_buf += stderr
                if len(err_buf) >= min_bufsize:
                    stderr_h.write(err_buf)
                    err_buf = ''
                elif '\n' in err_buf:
                    b = err_buf.split('\n')
                    if len(b) > 1:
                        for line in b[:-1]:
                            stderr_h.write(line)
                        err_buf = b[-1]
                    else:
                        stderr_h.write(b[0])
                        err_buf = ''

                all_err.append(stderr)

        if out_buf:
            all_out.append(out_buf)
            stdout_h.write(out_buf)
        if err_buf:
            all_err.append(err_buf)
            stderr_h.write(err_buf)

    except OSError as ose:
        if ose.errno == 2:  # No such file or directory
            logging.error("Could not find program for executing {}: {}".format(command[0] if isinstance(command, list)
                                                                              else command, ose.strerror))
        raise ProcessException(command, ose.errno, '', ose.strerror)

    finally:
        if shell_proc and shell_proc.stdout:
            shell_proc.stdout.close()
        if shell_proc and shell_proc.stderr:
            shell_proc.stderr.close()

    if shell_proc.returncode != 0:
        raise ProcessException(command, shell_proc.returncode, ''.join(all_err), ''.join(all_out))
    out, err = ''.join(all_out), ''.join(all_err)
    return out, err if err != out else ''
